//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#define CATCH_CONFIG_ENABLE_ALL_STRINGMAKERS
#include "catch2/catch.hpp"

#include "Tokenizer.h"

#include "spdlog/spdlog.h"

#include <filesystem>
#include <utility>
#include <iostream>
#include <fstream>

using namespace Catch::Matchers;



TEST_CASE( "Empty Tokenizer", "[tokenizer]" )
{
    REQUIRE(Compiler::Tokenizer::value() == 42);
}

