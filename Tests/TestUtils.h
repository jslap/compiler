//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#ifndef COMPILERMAIN_TESTUTILS_H
#define COMPILERMAIN_TESTUTILS_H

#include "FileUtils.h"

#define CATCH_CONFIG_ENABLE_ALL_STRINGMAKERS
#include "catch2/catch.hpp"

#define FMT_STRING_ALIAS 1
#include <fmt/core.h>

namespace Catch {
    template<>
    struct StringMaker<Compiler::FilePos> {
        static std::string convert( Compiler::FilePos const& value ) {
            return fmt::format("FilePos(\"{}\",{},{})", value.m_fileName.string(), value.m_lineNum, value.m_Column);
        }
    };
}
class TestUtils
{

};


#endif //COMPILERMAIN_TESTUTILS_H
