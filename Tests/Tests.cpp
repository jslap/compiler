//
// Created by Jean-Simon Lapointe on 2020-09-06.
//

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch2/catch.hpp"
#include "spdlog/spdlog.h"

static int setLogLevel = [](){
spdlog::set_level(spdlog::level::warn); //debug , warn
return 0;
}();
