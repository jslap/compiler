//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#define CATCH_CONFIG_ENABLE_ALL_STRINGMAKERS
#include "catch2/catch.hpp"

#include "SingleFileView.h"
#include "TestUtils.h"

TEST_CASE( "Single File View Basic Test", "[SingleFileView]" )
{

    std::string const content =
            "This is the content Line 1\n"
            "This is the content Line 2\n"
            "This is the content Line 3\n";

    Compiler::SingleFileView myBuffer{"", content};

    SECTION("Misc offsets")
    {
        CHECK(myBuffer.getPosition(0) == Compiler::FilePos{"", 0, 0}); // T
        CHECK(myBuffer.getPosition(26) == Compiler::FilePos{"", 0,26}); // \n
        CHECK(myBuffer.getPosition(27) == Compiler::FilePos{"", 1,0}); // T
        CHECK(myBuffer.getPosition(28) == Compiler::FilePos{"", 1,1}); // h

        CHECK(myBuffer.getPosition(52) == Compiler::FilePos{"", 1,25}); //
        CHECK(myBuffer.getPosition(53) == Compiler::FilePos{"", 1,26}); // \n
        CHECK(myBuffer.getPosition(54) == Compiler::FilePos{"", 2,0}); // T

        CHECK(myBuffer.getPosition(80) == Compiler::FilePos{"", 2,26}); // last \n
    }
}
