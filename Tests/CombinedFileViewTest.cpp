//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#define CATCH_CONFIG_ENABLE_ALL_STRINGMAKERS
#include "catch2/catch.hpp"

#include "CombinedFileView.h"
#include "TestUtils.h"

TEST_CASE( "Combined File View Tests", "[CombinedFileView]" )
{

    std::string const content1 =
            "This is the content Line 1\n"
            "This is the content Line 2\n"
            "This is the content Line 3\n";

    std::string const content2 =
            "This is the content2 Line 1\n"
            "This is the content2 Line 2\n"
            "This is the content2 Line 3\n";


    Compiler::SingleFileView myBuffer1{"File1", content1};
    Compiler::SingleFileView myBuffer2{"File2", content2};

    Compiler::CombinedFileView combFVSingle{myBuffer1};
    Compiler::CombinedFileView combFV{myBuffer1};
    const std::size_t insertPos = 27;
    const std::size_t insertSize = 27;
    combFV.addFile(insertPos, insertSize, Compiler::CombinedFileView{myBuffer2});

    std::string combFVStr = content1;
    combFVStr.replace(27, 27, content2);


    SECTION("Is Valid Insertion")
    {
        SECTION("Alone")
        {
            CHECK(combFVSingle.length() == 81);

            CHECK(combFVSingle.isValidInsertion(0,1));
            CHECK(combFVSingle.isValidInsertion(0,81));
            CHECK(!combFVSingle.isValidInsertion(0,82));

            CHECK(combFVSingle.isValidInsertion(80,1));
            CHECK(!combFVSingle.isValidInsertion(81,1));
        }

        SECTION("With one other")
        {
            CHECK(combFV.length() == content1.length()+content2.length()-insertSize);

            CHECK(combFV.isValidInsertion(0,1));
            CHECK(combFV.isValidInsertion(0,10));
            CHECK(combFV.isValidInsertion(0,27));

            CHECK(!combFV.isValidInsertion(0,28));
            CHECK(!combFV.isValidInsertion(20,28));
            CHECK(!combFV.isValidInsertion(27,1));
            CHECK(!combFV.isValidInsertion(27,40));
            CHECK(!combFV.isValidInsertion(28,40));
            CHECK(!combFV.isValidInsertion(52,1));
            CHECK(!combFV.isValidInsertion(53,1));
            CHECK(combFV.isValidInsertion(54,1));
            CHECK(combFV.isValidInsertion(54,11));
        }
    }

    SECTION("Iterator test")
    {
        SECTION("Alone")
        {
            auto iter = combFVSingle.cbegin();
            CHECK(iter != combFVSingle.cend());
            CHECK(*iter == 'T');
            iter++;
            CHECK(*iter == 'h');
            std::string fileCopy{combFVSingle.cbegin(), combFVSingle.cend()};
            CHECK(fileCopy == content1);
        }

        SECTION("With two")
        {
            std::string fileCopy{combFV.cbegin(), combFV.cend()};
            CHECK(fileCopy == combFVStr);
            auto iter = combFVSingle.cbegin();
            CHECK(iter != combFVSingle.cend());
            CHECK(*iter == 'T');
            iter++;
            CHECK(*iter == 'h');
        }
        SECTION("decremnt begin")
        {
            auto iter = combFVSingle.cbegin();
            CHECK_THROWS(iter--);
        }
        SECTION("decremnt end")
        {
            auto iter = combFVSingle.cend();
            iter--;
            CHECK(*iter == combFVStr.back());
        }
        SECTION("Increment and go back by 1")
        {
            auto iter = combFV.cbegin();
            for (int i = 0; i < combFVStr.length(); i++)
            {
                INFO("After incrementing " << i << " times");
                CHECK(*iter == combFVStr[i]);
                iter++;
            }
            CHECK(iter == combFV.cend());
            iter--;
            for (int i = 0; i < combFVStr.length(); i++)
            {
                INFO("From the end, After decrementing " << i+1 << " times");
                CHECK(*iter == combFVStr[combFVStr.length()-(i+1)]);
                if (iter != combFV.cbegin())
                    iter--;
            }
            CHECK(iter == combFV.cbegin());
        }
        SECTION("Advance to last.")
        {
            auto iter = combFV.cbegin();
            auto lastIter = std::next(iter, combFVStr.length()-1);
            CHECK(*lastIter == combFVStr.back());
        }
        SECTION("decremnt")
        {
            auto iter = combFVSingle.cbegin();
            CHECK(iter != combFVSingle.cend());
            CHECK(*iter == 'T');
            iter++;
            CHECK(*iter == 'h');
            iter--;
            CHECK(*iter == 'T');

        }

    }

    SECTION("Complex combination")
    {
        std::string s1 = "abcdefghijklmnop";
        std::string s2 = "qrstuvwxyz";
        std::string s3 = "0123456789";



        Compiler::SingleFileView myBuffer1{"File1", s1};
        Compiler::SingleFileView myBuffer2{"File2", s2};
        Compiler::SingleFileView myBuffer3{"File3", s3};

        Compiler::CombinedFileView c1{myBuffer1};
        Compiler::CombinedFileView c2{myBuffer2};
        Compiler::CombinedFileView c3{myBuffer3};
        c2.addFile(4, 1, c3);
        c1.addFile(4, 1, c2);

        std::string sres2 = s2;
        sres2.replace(4, 1, s3);

        std::string sres1 = s1;
        sres1.replace(4, 1, sres2);
        CHECK(c1.length() == sres1.length());
        CHECK(c1.toString() == sres1);
    }

    SECTION("Misc offsets")
    {
        SECTION("Before file2")
        {
            CHECK(combFV.getPosition(0) == Compiler::FilePos{"File1", 0, 0}); // T
            CHECK(combFV.getPosition(insertPos-1) == Compiler::FilePos{"File1", 0,26}); // \n
        }

        SECTION("Inside file2")
        {
            CHECK(combFV.getPosition(insertPos) == Compiler::FilePos{"File2", 0,0}); // T
            CHECK(combFV.getPosition(insertPos+28) == Compiler::FilePos{"File2", 1,0}); // T
            CHECK(combFV.getPosition(insertPos+83) == Compiler::FilePos{"File2", 2,27}); // T
        }

        SECTION("After file2")
        {
            CHECK(combFV.getPosition(insertPos+84) == Compiler::FilePos{"File1", 2,0}); // T
        }
    }
}
