cmake_minimum_required(VERSION 3.17)
project(compilerLibTests)


add_executable(compilerLibTests
        TokenizerTests.cpp
        Tests.cpp
        SingleFileViewTest.cpp TestUtils.cpp TestUtils.h CombinedFileViewTest.cpp)

target_include_directories(compilerLibTests PRIVATE ../lib)
target_link_libraries(compilerLibTests compilerLib)

if(NOT DEFINED ENV{CI})
    target_link_libraries(compilerLibTests spdlog)
    target_link_libraries(compilerLibTests fmt)
endif()
