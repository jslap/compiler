//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#ifndef COMPILERMAIN_SINGLEFILEVIEW_H
#define COMPILERMAIN_SINGLEFILEVIEW_H

#include "FileUtils.h"

#include <filesystem>

namespace Compiler
{
    class SingleFileView
    {
    public:
        explicit SingleFileView(std::filesystem::path const& fileName);
        explicit SingleFileView(std::filesystem::path const& fileName, std::string const & content);

        FilePos getPosition(std::size_t pos) const;
        std::size_t length() const {return m_content.length();}
        const std::string& getContent() const {return m_content;}


    private:
        std::filesystem::path m_fileName;
        std::string m_content;
    };
}



#endif //COMPILERMAIN_SINGLEFILEVIEW_H
