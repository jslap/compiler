//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#include "SingleFileView.h"

#include <fstream>
#include <stdexcept>
#include <range/v3/view.hpp>
#include <range/v3/algorithm/count.hpp>

namespace
{
    std::string loadFile(std::filesystem::path const &fileName)
    {
        if (!fileName.empty())
        {
            std::ifstream str{fileName};
            return std::string((std::istreambuf_iterator<char>(str)),
                               std::istreambuf_iterator<char>());
        }
        return {};
    }

}

using Compiler::SingleFileView;
using Compiler::FilePos;

SingleFileView::SingleFileView(std::filesystem::path const &fileName):
        m_fileName(fileName),
        m_content(loadFile(fileName))
{
}

SingleFileView::SingleFileView(std::filesystem::path const &fileName, std::string const &content):
        m_fileName(fileName),
        m_content(content)
{
}

FilePos SingleFileView::getPosition(std::size_t pos) const
{
    if (pos >= m_content.size())
        throw std::range_error("SingleFileView::getPosition: index out of range.");
    auto cumulLineLengths = m_content
                            | ranges::views::split('\n')
                            | ranges::views::transform([](auto const & s){return ranges::to<std::string>(s).length() + 1;})
                            | ranges::views::partial_sum
                            | ranges::views::take_while([&pos](auto const n){return n < pos+1;})
                            | ranges::to<std::vector<int>>;

    if (cumulLineLengths.empty()) // on first line.
        return {m_fileName, 0, pos};
    return {m_fileName, cumulLineLengths.size(), pos-cumulLineLengths.back()};
}

