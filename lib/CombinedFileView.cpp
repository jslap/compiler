//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#include "CombinedFileView.h"

#include <numeric>

Compiler::CombinedFileView::CombinedFileView(Compiler::SingleFileView const &fileView):
        m_mainFileView{&fileView}
{

}

void Compiler::CombinedFileView::addFile(std::size_t pos, std::size_t len, Compiler::CombinedFileView const &fileView)
{
    if (!isValidInsertion(pos, len))
        throw std::logic_error("CombinedFileView::addFile: isValidInsertion is false");

    m_insertions[pos] = Insertion{pos, len, &fileView};
}

Compiler::FilePos Compiler::CombinedFileView::getPosition(std::size_t pos) const
{
    std::size_t posInMain = 0;
    for (auto ins : m_insertions)
    {
        if (pos < ins.second.m_pos-posInMain)
            return Compiler::FilePos(m_mainFileView->getPosition(posInMain + pos));
        pos -= ins.second.m_pos;
        if (pos < ins.second.m_fileView->length())
            return Compiler::FilePos(ins.second.m_fileView->getPosition(pos));
        pos -= ins.second.m_fileView->length();
        posInMain = ins.second.m_pos + ins.second.m_len;
    }
    return Compiler::FilePos(m_mainFileView->getPosition(posInMain + pos));
}

bool Compiler::CombinedFileView::isValidInsertion(std::size_t pos, std::size_t len) const
{
    if (len == 0)
        return false;
    if (pos + len > m_mainFileView->getContent().length())
        return false;
    if (m_insertions.empty())
        return true;

    InsMap::const_iterator afterPos = m_insertions.lower_bound(pos);

    // Check that the insertion would not go pass the next
    if (afterPos != m_insertions.end())
    {
        if (pos + len > afterPos->second.m_pos)
            return false;
    }

    // check that the previous insertion would not go pass this one.
    if (afterPos != m_insertions.begin())
    {
        InsMap::const_iterator prevElt = (afterPos != m_insertions.end()) ? std::prev(afterPos) : m_insertions.begin();
        if (prevElt->second.m_pos + prevElt->second.m_len > pos)
            return false;
    }

    // Else, it is valid.
    return true;
}

std::size_t Compiler::CombinedFileView::length() const
{
    return std::transform_reduce(m_insertions.begin(), m_insertions.end(),
                                 m_mainFileView->length(), std::plus<>(), [](auto mapPair){
                Insertion const& ins = mapPair.second;
                return static_cast<std::size_t>(ins.m_fileView->length()-ins.m_len);
            });
}

std::string Compiler::CombinedFileView::toString() const
{
    return std::string(cbegin(), cend());
}

std::string::value_type Compiler::CombinedFileView::getCharacter(std::size_t pos) const
{
    return m_mainFileView->getContent()[pos];
}

Compiler::CombinedFileView::Iterator::PosDef Compiler::CombinedFileView::getPtrForPos(std::size_t pos) const
{
    std::size_t posInMain = 0;
    for (auto ins : m_insertions)
    {
        if (pos < ins.second.m_pos-posInMain)
            return {m_mainFileView->getContent().c_str()+ posInMain,
                    m_mainFileView->getContent().c_str()+ posInMain + pos,
                    m_mainFileView->getContent().c_str()+ ins.second.m_pos };

        pos -= ins.second.m_pos;
        if (pos < ins.second.m_fileView->length())
            return ins.second.m_fileView->getPtrForPos(pos);
        pos -= ins.second.m_fileView->length();
        posInMain = ins.second.m_pos + ins.second.m_len;
    }
    if (posInMain + pos >= m_mainFileView->getContent().length())
        return {};
    return {m_mainFileView->getContent().c_str()+ posInMain,
            m_mainFileView->getContent().c_str()+ posInMain + pos,
            m_mainFileView->getContent().c_str() + m_mainFileView->getContent().length()};
}

void Compiler::CombinedFileView::Iterator::resetPos(std::size_t pos)
{
    m_posDef = m_ref->getPtrForPos(pos);
}
