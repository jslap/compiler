//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#ifndef COMPILERMAIN_FILEUTILS_H
#define COMPILERMAIN_FILEUTILS_H

#include <filesystem>

namespace Compiler
{
    class FilePos
    {
    public:
        std::filesystem::path m_fileName;
        std::size_t m_lineNum;
        std::size_t m_Column;

        bool operator==(FilePos const & rhs) const
        {
            return std::make_tuple(m_fileName, m_lineNum, m_Column) == std::make_tuple(rhs.m_fileName, rhs.m_lineNum, rhs.m_Column);
        }
    };

}


#endif //COMPILERMAIN_FILEUTILS_H
