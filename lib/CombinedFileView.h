//
// Created by Jean-Simon Lapointe on 2021-02-27.
//

#ifndef COMPILERMAIN_COMBINEDFILEVIEW_H
#define COMPILERMAIN_COMBINEDFILEVIEW_H

#include "FileUtils.h"
#include "SingleFileView.h"

#include <map>
#include <iterator> // For std::forward_iterator_tag
#include <cstddef>  // For std::ptrdiff_t

namespace Compiler
{
    class CombinedFileView
    {
    public:
        explicit CombinedFileView(SingleFileView const & fileView);

        void addFile(std::size_t pos, std::size_t len, CombinedFileView const& fileView);
        bool isValidInsertion(std::size_t pos, std::size_t len) const;

        FilePos getPosition(std::size_t pos) const;
        std::string::value_type getCharacter(std::size_t pos) const;
        std::size_t length() const;

        std::string toString() const;

        struct Iterator
        {
            friend class CombinedFileView;

            using iterator_category = std::random_access_iterator_tag;
            using difference_type   = std::ptrdiff_t;
            using value_type        = std::string::value_type;
            using pointer           = std::string::value_type const*;  // or also value_type*
            using reference         = std::string::value_type const&;  // or also value_type&

            struct End{};
            Iterator(CombinedFileView const * ref, End ): m_ref{ref}, m_curPos{ref->length()}
            {
            }
            Iterator(CombinedFileView const * ref): m_ref{ref}, m_curPos{0}
            {
                resetPos(0);
            }
            Iterator() = default;

            reference operator*() const { return *m_posDef.m_curPtr; }
            pointer operator->() { return m_posDef.m_curPtr; }

            Iterator& operator++(){ increment(1); return *this;}
            Iterator operator++(int) { Iterator tmp = *this; increment(1); return tmp; }
            Iterator& operator--(){ increment(-1); return *this;}
            Iterator operator--(int) { Iterator tmp = *this; increment(-1); return tmp; }
            inline Iterator& operator+=(difference_type rhs) {increment(rhs); return *this;}
            inline Iterator& operator-=(difference_type rhs) {increment(-rhs);; return *this;}
            inline difference_type operator-(const Iterator& rhs) const {return m_curPos-rhs.m_curPos;}

            inline Iterator operator+(difference_type rhs) const {Iterator tmp{*this}; tmp.increment(rhs); return tmp;}
            inline Iterator operator-(difference_type rhs) const {Iterator tmp{*this}; tmp.increment(-rhs); return tmp;}
            friend inline Iterator operator+(difference_type lhs, const Iterator& rhs) {Iterator tmp{rhs}; tmp.increment(lhs); return tmp;}
            friend inline Iterator operator-(difference_type lhs, const Iterator& rhs) {Iterator tmp{rhs}; tmp.increment(-lhs); return tmp;}

            friend bool operator== (const Iterator& a, const Iterator& b){return a.m_posDef.m_curPtr == b.m_posDef.m_curPtr;};
            friend bool operator!= (const Iterator& a, const Iterator& b) { return a.m_posDef.m_curPtr != b.m_posDef.m_curPtr; };
        private:
            void resetPos(std::size_t pos);
            void increment(difference_type diff)
            {
                if (diff < 0 && m_curPos < -diff)
                    throw std::range_error("decrement iter before begin()");
                m_curPos += diff;
                if (m_posDef.m_curPtr + diff < m_posDef.m_curPtrEnd &&
                        m_posDef.m_curPtr + diff >= m_posDef.m_basePtr)
                    m_posDef.m_curPtr += diff;
                else
                    resetPos(m_curPos);
            }

            struct PosDef
            {
                std::string::value_type const* m_basePtr{};
                std::string::value_type const* m_curPtr{};
                std::string::value_type const* m_curPtrEnd{};
            };
            CombinedFileView const * m_ref{};
            PosDef m_posDef{};
            std::size_t m_curPos{};
        };

        Iterator cbegin() const {return Iterator{this};}
        Iterator cend() const {return Iterator{this, Iterator::End{}};}

    private:
        Iterator::PosDef getPtrForPos(std::size_t pos) const;

        SingleFileView const * m_mainFileView;
        struct Insertion
        {
            std::size_t m_pos;
            std::size_t m_len;
            CombinedFileView const* m_fileView;
        };
        using InsMap = std::map<std::size_t, Insertion>;
        InsMap m_insertions;
    };

}




#endif //COMPILERMAIN_COMBINEDFILEVIEW_H
