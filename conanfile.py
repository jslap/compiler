from conans import ConanFile, CMake
import os

class MyVimConan(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    # noinspection PyInterpreter
    requires = "catch2/[>=2.11]", \
               "range-v3/[>=0.11]", \
               "fmt/[=7.1.2]", \
               "spdlog/[=1.8.2]"
    generators = "cmake"

    def configure(self):
        if  ("CI" in os.environ):
            self.options["spdlog"].header_only = True
            self.options["fmt"].header_only = True

