cmake_minimum_required(VERSION 3.17)
project(compilerMain)

#set(CMAKE_VERBOSE_MAKEFILE ON)

set(CMAKE_CXX_STANDARD 20)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

#add_executable(myvim main.cpp )
#target_link_libraries(myvim myvimlib)

#if(NOT DEFINED ENV{CI})
#    target_link_libraries(myvim spdlog)
#    target_link_libraries(myvim fmt)
#endif()

add_subdirectory(lib)
add_subdirectory(Tests)
